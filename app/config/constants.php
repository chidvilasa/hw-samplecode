<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

return array(
    'essentials_path' => url() . '/essentials',
    'roles' => array(
        '1' => 'Super Admin',
        '2' => 'Office Staff',
        '3' => 'Dealer',
        '4' => 'Tinter',
        '5' => 'Manager',
        '6' => 'Salesperson',
        '7' => 'Dealership Invoicing'
    ),
    'profile_images' => url() . '/essentials/uploads/users',
    'third_party' => url() . '/packages/thirdparty',
    'profile_real_path' => '/essentials/uploads/users/',
    'invoices_pdfs_path' => '/essentials/invoices/',
    'payroll_files_path' => '/essentials/payrolls/',
    'reports_files_path' => '/essentials/reports/',
    'third_part_real' => '/app/Libraries/Thirdparty',
    'payroll_roles' => array(4, 5, 6),
    'reports_roles' => array(3, 4, 5, 6),
    'sample_csv_files_path' => '/essentials/sampleCsv/'
);


