@extends('layouts.master')
@section('body_content')
<section class="content">

    <div class="box">
        <div class="box-header">
            <div class="row">
                <div class="col-lg-8"><h3 class="box-title">Products</h3></div>
                <div class="col-lg-4 box-button" align="right"><button class="btn btn-success" data-toggle="modal" data-target="#myModal0" onclick="callPopup(1);"><i class="fa fa-plus" ></i> &nbsp;Add</button></div>
                <div class="clearfix"></div>
            </div>

            <div class="div_line"></div>
        </div><!-- /.box-header -->
        <div id="popup_div">

        </div>
        <div class="box-body table-responsive" id="products_grid">

        </div><!-- /.box-body -->
    </div>
</section><!-- /.content -->
<!-- page script -->
<script type="text/javascript">
    var form_id = form_id;
    $(document).ready(function () {
        callProducts();
    });


    /**
     * This function is used to call the products Grid.
     * @param 
     * @returns html
     */
    function callProducts()
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/products/get_products",
            async: true,
            beforeSend: function () {
                $('#products_grid').html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },
            success: function (data) {
                $('#products_grid').html('');
                $('#products_grid').html(data.data);
            }
        });
    }

    /**
     * This function is used to fire the popup
     * @param {int} popupMode
     * @param {int} productId
     * @returns Html    
     *
     **/
    function callPopup(popupMode, productId)
    {
        var params = '';
        if (typeof (productId) !== 'undefined')
        {
            params = {mode: popupMode, productId: productId};
        }
        else
        {
            params = {mode: popupMode};
        }
        $.ajax({
            url: '/products/popup',
            dataType: 'json',
            type: 'POST',
            data: params,
            success: function (data) {
                if (data.dialog) {
                    var $modal = $(data.dialog);
                    $('#popup_div').append($modal);
                    $modal.filter('.modal').modal();
                } else {
                    alert(data.dialog);
                }

            }
        });
    }

    /**
     * This function will be used to call the change status global function. 
     * @param {int} type
     * @param {int} recordId
     * @param {string} table
     * @param {string} column
     * @returns void
     */
    function callChangeStatus(type, recordId, table, column)
    {
        var status = (type === 0) ? 'deactivate' : 'activate';
        if (confirm('Are sure you want to ' + status + ' the product?'))
        {
            changeStatus(type, recordId, table, column, 'callProducts');
        }
    }

</script>
@stop