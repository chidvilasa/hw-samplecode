@if(is_array($grid_data) && !empty($grid_data))
<table id="products" class="table table-bordered table-striped">
    <thead>
        <tr>
            <th width="50%" style="display:none;">id</th>
            <th width="20%">Product Order</th>
            <th width="35%">Product Name</th>
            <th width="15%">Updated On</th>
            <th width="10%">Status</th>
            <th width="20%">Actions</th>
        </tr>
    </thead>    
    <tbody>
        @foreach($grid_data as $data)
        <tr>
            <td style="display:none;"> </td>
            <td><?php echo $data->job_order; ?></td> 
            <td><?php echo $data->job_name; ?></td>				
            <td><?php echo date('m/d/Y', strtotime($data->updated_at)); ?></td>
            <td><?php echo ($data->status == 1) ? 'Active' : 'Deactive'; ?></td>
            <td><a class="btn btn-primary btn-xs" data-toggle="modal" data-target="#myModal<?php echo $data->id; ?>" onclick="callPopup(2,<?php echo $data->id; ?>);"><i class="fa fa-edit" ></i> &nbsp;Edit</a>
                @if($data->status == 1)
                <a class="wid80 btn btn-danger btn-xs" onclick="callChangeStatus(0,<?php echo $data->id; ?>, 'Products', 'Status');"><i class="fa fa-times"></i> &nbsp;Deactivate</a>
                @else
                <a class="wid80 btn btn-success btn-xs" onclick="callChangeStatus(1,<?php echo $data->id; ?>, 'Products', 'Status');"><i class="fa fa-check"></i> &nbsp;Activate</a>
                @endif
            </td>
        </tr>
        @endforeach
    </tbody>

</table>
@else
<div align="center">No records found.</div>
@endif
<script type="text/javascript">
    $(document).ready(function () {
        $('#products').dataTable({
            "bPaginate": true,
            "bLengthChange": true, // This is for search bar over the grid.
            "bFilter": true, //This is for number of records per page.
            "bSort": true,
            "bInfo": true,
            "bAutoWidth": false,
            "iDisplayLength": 100,
            "aoColumnDefs": [
                {'bSortable': false, 'aTargets': [5]},
                {"bSearchable": false, 'aTargets': [5]}
            ]
        });
    });

</script>