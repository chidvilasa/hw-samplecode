<div class="modal fade" id="myModal<?php echo $product_data['form_id']; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal mart15 popup_form" role="form" id="product_form_<?php echo $product_data['form_id']; ?>" name="product_form">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel"><?php echo ($product_data['mode'] == 1) ? 'Add' : 'Edit'; ?> Product</h4>
                </div>            
                <div class="modal-body">                   
                    <div class="alert alert-danger alert-dismissable" id="error_div" style="display: none;" > </div>
                    <input type="hidden" id="mode" name="mode" value="<?php echo $product_data['mode']; ?>" />
                    <input type="hidden" id="product_id" name="product_id" value="<?php echo $product_data['form_id']; ?>" />
                    <div class="form-group mart30">
                        <label for="jobname" class="control-label col-sm-4">Product Name:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="" id="product_name" name="product_name" value="<?php echo (isset($product_data['product_details']) && !empty($product_data['product_details'])) ? $product_data['product_details']->job_name : ''; ?>">
                            <div id="product_name_err" ></div>
                        </div>                        
                    </div>
                    <div class="form-group mart30">
                        <label for="product_order" class="control-label col-sm-4">Product Order:</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" placeholder="" id="product_order" name="product_order" value="<?php echo (isset($product_data['product_details']) && !empty($product_data['product_details'])) ? $product_data['product_details']->job_order : $product_data['lastOrderId']; ?>">
                            <div id="product_order_err" ></div>
                        </div>                        
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success"  ><?php echo ($product_data['mode'] == 1) ? 'Add' : 'Update'; ?></button> 
                </div>
            </div>
        </form>  
    </div>
    <script type="text/javascript">
        var formId = <?php echo $product_data['form_id']; ?>;
        $(document).ready(function () {

            /* The below code is triggered when user clicks on the close button.*/
            $(document).on('hide.bs.modal', "[id^='myModal']", function () {
                cleanPopupDiv('popup_div');
            });

            $.validator.addMethod("integers", function (value, element) {
                return this.optional(element) || /^\+?\d+$/.test(value);
            }, "Username must contain only numbers.");

            var form = $("#product_form_" + formId).get(0);
            $.removeData(form, 'validator');
            $("#product_form_" + formId).validate({
                rules: {
                    product_name: {
                        required: true,
                        maxlength: 40
                    },
                    product_order: {
                        integers: true
                    }
                },
                messages: {
                    product_name: {
                        required: "Please enter product name."
                    },
                    product_order: {
                        integers: "Please enter integers only."
                    }
                },
                errorPlacement: function (error, element) {
                    element = $("#" + element.attr("id") + "_err");
                    $(element).html('');
                    error.appendTo($(element));
                },
                submitHandler: function () {
                    $.ajax({
                        url: '/products/update_products',
                        dataType: 'json',
                        type: 'POST',
                        data: $("#product_form_" + formId).serialize(),
                        success: function (data) {
                            if (data.status === 200)
                            {
                                $(".modal").css('display', 'none');
                                $(".modal-backdrop").remove();
                                location.reload(); //This method is used to avoid the issue with the product name reedit issue.
                            }
                            else
                            {
                                $("#error_div").html(data.message);
                                $("#error_div").show();
                            }
                        }
                    });
                }
            });
        });
    </script>
</div>