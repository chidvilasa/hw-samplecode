<!-- Bootstrap -->
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/bootstrap.min.js" type="text/javascript"></script> 
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
<!-- AdminLTE App -->
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/AdminLTE/app.js" type="text/javascript"></script>
<script type="text/javascript">

    var mainSegment = "<?php echo Request::segment(1); ?>";
    $(".sidebar-menu>li.active").removeClass("active");
    $("#" + mainSegment + "_menu").addClass("active");

    /**
     * This function will return today date as "mm/dd/yyyy" format     
     * @returns string
     */
    function getToday()
    {
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();
        if (dd < 10) {
            dd = '0' + dd
        }
        if (mm < 10) {
            mm = '0' + mm
        }
        return today = mm + '/' + dd + '/' + yyyy;
    }

    /**
     * This function will give the last week date from today.
     * @returns date
     */
    function getLastWeekDate(range)
    {
        var period = 7;
        if (typeof range == 'undefined')
        {
            range = 1;
        }

        switch (range)
        {
            case 1:
                period = 7;
                break;
            case 2:
                period = 30;
                break;
        }

        var date = new Date();
        date.setDate(date.getDate() - period);

        var month = date.getMonth() + 1;
        var dayDate = date.getDate();
        var modifiedDate = ((month < 10) ? '0' + month : month) + '/' + ((dayDate < 10) ? '0' + dayDate : dayDate) + '/' + date.getFullYear();

        return modifiedDate;
    }

    /**
     * This function will send an Ajax request to the server to change the status of the item.
     * @param {int} type
     * @param {int} recordId
     * @param {string} table
     * @param {string} column
     * @param {string} callFunction
     * @returns void
     */
    function changeStatus(type, recordId, table, column, callFunction, functionParams)
    {
        $.ajax({
            type: "POST",
            dataType: "json",
            url: "/change_status",
            data: {type: type, recordId: recordId, tableName: table, column: column},
            /*beforeSend: function(){
             $('#products_grid').html('<img src="<?php #echo Config::get('constants.essentials_path');   ?>/img/ajax-loader.gif" class="loader_position" />');                            
             },*/
            success: function (data) {
                if (data.status === 200)
                {
                    makeDynamicFunctionCall(callFunction, functionParams);
                }
                else
                {
                    alert(data.message);
                }
            }
        });
    }

    /**
     * This function is used to call the Dyamic function from the changeStatus function. 
     * @param {string} functionName
     * @returns void
     */
    function makeDynamicFunctionCall(functionName, functionParams)
    {
        if (typeof functionParams != 'undefined')
        {
            this[functionName](functionParams['role'], functionParams['div_id'], functionParams['url']);
        }
        else
        {
            this[functionName]();
        }
    }


    /**
     * This function is used to clear the popup div when user click the close button
     * @param string divName
     * @returns void
     */
    function cleanPopupDiv(divName)
    {
        var divId = $('.modal').prop("id");
        $("#" + divName).remove('#' + divId);
        $('#' + divId).remove();
    }

    /**
     * This function is used to call the Dyamic function from the changeAssignment function. 
     * @param {string} name functionName
     * @param {array} name functionParams
     * @returns void
     */
    function makeDynamicTabLoad(functionName, functionParams)
    {
        if (typeof functionParams !== 'undefined')
        {
            this[functionName].apply(null, functionParams);
        }
        else
        {
            this[functionName]();
        }
    }

    /**
     * This function is used to load the requested Tab.
     * @param {type} userData
     * @param {type} type
     * @param {type} confirm_label
     * @param {type} dynamic_content
     * @param {type} dynamic_function
     * @returns {undefined}     */
    function loadTab(id, url, tabName, tab, div_id)
    {
        var requiredUrl = url + id + "/" + tab + "/" + tabName;
        $.ajax({
            datatype: "json",
            type: "GET",
            url: requiredUrl,
            beforeSend: function () {
                $('#' + div_id).html('<img src="<?php echo Config::get('constants.essentials_path'); ?>/img/ajax-loader.gif" class="loader_position" />');
            },
            success: function (data) {
                $('#' + div_id).html('');
                $('#' + div_id).html(data.output);
            }
        });
    }



</script>
<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-59975609-1', 'auto');
    ga('send', 'pageview');

</script>
