<a href="/" class="logo">
    <!-- Add the class icon to your logo image or logo icon to add the margining -->
    <img src="<?php echo Config::get('constants.essentials_path'); ?>/img/logo.png" alt="Powered by APD" title="Powered by APD" />
</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>
    <div class="navbar-right">
        <ul class="nav navbar-nav">
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <span><?php echo $menuitems->first_name . ' ' . $menuitems->last_name; ?> <i class="caret"></i></span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                        <img src="<?php echo ($menuitems->profile_image != '') ? Config::get('constants.profile_images') . '/thumbs/' . $menuitems->profile_image : Config::get('constants.profile_images') . '/default_avatar.png'; ?>" class="img-circle" alt="User Image" />
                        <p>
                            <?php $roles_array = Config::get('constants.roles'); ?>
                            <?php echo $menuitems->first_name . ' ' . $menuitems->last_name; ?> - <?php print_r($roles_array[$menuitems->role_id]); ?>
                            <small>Member since <?php echo date("M. Y", strtotime($menuitems->created_at)); ?></small>
                        </p>
                    </li>
                    <!-- Menu Body -->                                
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="/profile" class="btn btn-default btn-flat">Profile</a>
                        </div>
                        <div class="pull-right">
                            <a href="/logout" class="btn btn-default btn-flat">Log Out</a>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</nav>
