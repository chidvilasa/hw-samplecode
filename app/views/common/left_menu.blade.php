<!-- sidebar: style can be found in sidebar.less -->
<section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
        <div class="pull-left image">
            <img src="<?php echo ($menuitems->profile_image != '') ? Config::get('constants.profile_images') . '/thumbs/' . $menuitems->profile_image : Config::get('constants.profile_images') . '/default_avatar.png'; ?>" class="img-circle" alt="User Image" />
        </div>
        <div class="pull-left info">
            <p>Hello, <?php echo $menuitems->first_name; ?></p>
            <?php $roles_array = Config::get('constants.roles'); ?>
            <em><?php print_r($roles_array[$menuitems->role_id]); ?></em>
        </div>
    </div>    
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu">
        @if($menuitems->role_id == 1)
        <li id="manage_users_menu">
            <a href="/manage_users">
                <i class="fa fa-user"></i> <span>Manage Users</span>
            </a>
        </li>        
        @endif
        @if($menuitems->role_id == 1 || $menuitems->role_id == 2)
        <li id="manage_cars_menu">
            <a href="/manage_cars">
                <i class="fa fa-car"></i> <span>Manage Cars</span>
            </a>
        </li>
        <li class="active" id="products_menu">
            <a href="/products">
                <i class="fa fa-wrench"></i> <span>Products</span>
            </a>
        </li>

        <li id="tinters_menu">
            <a href="/tinters">
                <i class="fa fa-fire-extinguisher"></i> <span>Tinters</span>
            </a>
        </li>

        <li id="dealers_menu">
            <a href="/dealers">
                <i class="fa fa-briefcase"></i> <span>Dealers</span>
            </a>
        </li>

        <li id="invoicing_menu">
            <a href="/invoicing">
                <i class="fa fa-file-o"></i> <span>Invoicing</span>
            </a>
        </li>

        <li id="managers_menu">
            <a href="javascript:void(0);">
                <i class="fa fa-user"></i> <span>Manager</span>
            </a>
        </li>

        <li id="salesperson_menu">
            <a href="javascript:void(0);">
                <i class="fa fa-users"></i> <span>Salesperson</span>
            </a>
        </li>

        @elseif($menuitems->role_id == 3 || $menuitems->role_id == 4 || $menuitems->role_id == 5 || $menuitems->role_id == 6)

        <li id="reports_menu">
            <a href="javascript:void(0);">
                <i class="fa fa-users"></i> <span>Reports</span>
            </a>
        </li>

        @endif

        @if($menuitems->role_id == 1 || $menuitems->role_id == 2)
        <li id="log_work_menu">
            <a href="/log_work">
                <i class="fa fa-pencil-square-o"></i> <span>Log Work</span>
            </a>
        </li>
        @endif

        @if($menuitems->role_id == 7)
        <li id="log_work_menu">
            <a href="/log_work/invoicing_log">
                <i class="fa fa-pencil-square-o"></i> <span>Work Log</span>
            </a>
        </li>
        @endif

        @if($menuitems->role_id == 3)
        <li id="dealers_menu">
            <a href="/dealers/work_log/<?php echo base64_encode($menuitems->id); ?>">
                <i class="fa fa-pencil-square-o"></i> <span>Work Log</span>
            </a>
        </li>
        @endif

        @if($menuitems->role_id != 4)
        <li id="invoices_menu">
            <a href="/invoices">
                <i class="fa fa-file-text"></i> <span>Invoices</span>
            </a>
        </li>
        @endif

        @if($menuitems->role_id == 1)
        <li id="payroll_menu">
            <a href="/payroll">
                <i class="fa fa-usd"></i> <span>Payroll</span>
            </a>
        </li>

        <li id="reports_menu">
            <a href="/reports">
                <i class="fa fa-list"></i> <span>Reports</span>
            </a>
        </li>
        @endif

        @if($menuitems->role_id == 4)
        <li id="tinters_menu">
            <a href="/tinters/work_log/<?php echo base64_encode($menuitems->id); ?>">
                <i class="fa fa-pencil-square-o"></i> <span>Work Log</span>
            </a>
        </li>
        @endif

    </ul>
</section>
<!-- /.sidebar -->