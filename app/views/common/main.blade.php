<meta charset="UTF-8">
<title>APD | {{ isset($pageTitle) ? $pageTitle : 'Dealership Invoicing' }}</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<!-- bootstrap 3.0.2 -->
<link href="<?php echo Config::get('constants.essentials_path'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<!-- font Awesome -->
<link href="<?php echo Config::get('constants.essentials_path'); ?>/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
<!-- Ionicons -->
<link href="<?php echo Config::get('constants.essentials_path'); ?>/css/ionicons.min.css" rel="stylesheet" type="text/css" />
<!-- Morris chart -->
<link href="<?php echo Config::get('constants.essentials_path'); ?>/css/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
<!-- Theme style -->
<link href="<?php echo Config::get('constants.essentials_path'); ?>/css/AdminLTE.css" rel="stylesheet" type="text/css" />
<link rel="shortcut icon" type="image/x-icon" href="<?php echo Config::get('constants.essentials_path'); ?>/img/favicon.ico">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/jquery.min.js"></script>
<script src="<?php echo Config::get('constants.essentials_path'); ?>/js/jquery.validate.js"></script>