<?php

/*
 * This file will consist of all common functions.
 * These function can be used any where in the application.
 * 
 */

/**
 * This function will encode the html data.
 * @param string $string
 * @return string
 */
function htmlEncode($string) {
    return htmlentities(trim($string), ENT_QUOTES, "UTF-8");
}

/**
 * This function will decode the html data.
 * @param string $string
 * @return string
 */
function htmlDecode($string) {
    return html_entity_decode($string, ENT_QUOTES);
}

/**
 * This function is used to identify the Character set and convert the string to UTF-8 supported string.
 * @param string $base_string
 * @return string
 */
function convertToUTF($base_string = '') {
    if ($base_string != '') {
        $base_string = mb_convert_encoding($base_string, "UTF-8", mb_detect_encoding($base_string, "UTF-8, ASCII, ISO-8859-1, ISO-8859-2, ISO-8859-3, ISO-8859-4, ISO-8859-5, ISO-8859-6, ISO-8859-7, ISO-8859-8, ISO-8859-9, ISO-8859-10, ISO-8859-13, ISO-8859-14, ISO-8859-15, ISO-8859-16, Windows-1251, Windows-1252, Windows-1254", true));
    }

    return $base_string;
}

function createAssignBlocks($userData, $type) {
    $userBlock = '';
    switch ($type) {
        case 1:
            //echo json_encode($userData, ENT_QUOTES);
            $userBlock = '<button id="' . $userData['confirm_label'] . '_' . $userData['user_id'] . '" class="btn btn-remove" onclick=\'changeAssignment(' . json_encode($userData, ENT_QUOTES) . ',"remove","' . $userData['confirm_label'] . '",' . json_encode($userData['dynamic_params'], ENT_QUOTES) . ',"' . $userData['dynamic_function'] . '");\' >' . $userData['name'] . '</button>';
            break;
        case 2:
            //echo json_encode($userData, ENT_QUOTES);
            $userBlock = '<button id="' . $userData['confirm_label'] . '_' . $userData['user_id'] . '" class="btn btn-add" onclick=\'changeAssignment(' . json_encode($userData, ENT_QUOTES) . ',"add","' . $userData['confirm_label'] . '",' . json_encode($userData['dynamic_params'], ENT_QUOTES) . ',"' . $userData['dynamic_function'] . '");\' >' . $userData['name'] . '</button>';
            break;
        case 3:
            $userBlock = '<button id="' . $userData['confirm_label'] . '_' . $userData['user_id'] . '" class="btn btn-warned" onclick=\'changeAssignment(' . json_encode($userData, ENT_QUOTES) . ',"deactivate","' . $userData['confirm_label'] . '",' . json_encode($userData['dynamic_params'], ENT_QUOTES) . ',"' . $userData['dynamic_function'] . '");\' >' . $userData['name'] . '</button>';
            break;
        case 4:
            $userBlock = '<button id="' . $userData['confirm_label'] . '_' . $userData['user_id'] . '" class="btn btn-warn" onclick=\'changeAssignment(' . json_encode($userData, ENT_QUOTES) . ',"activate","' . $userData['confirm_label'] . '",' . json_encode($userData['dynamic_params'], ENT_QUOTES) . ',"' . $userData['dynamic_function'] . '");\' >' . $userData['name'] . '</button>';
            break;
    }

    return $userBlock;
}

/**
 * This function will create the year block accoring to the condition.
 * @param array $yearsData
 * @param int $type
 * @return string
 */
function createYearBlock($yearsData, $type) {
    $yearsBlock = '';
    switch ($type) {
        case 1:
            //echo json_encode($userData, ENT_QUOTES);
            $yearsBlock = '<button id="data_' . $yearsData['year'] . '" class="btn btn-remove" onclick=\'changeYear(' . json_encode($yearsData, ENT_QUOTES) . ',"remove","' . $yearsData['label_name'] . '"); return false;\' >' . $yearsData['year'] . '</button>';
            break;
        /* case 2:
          //echo json_encode($userData, ENT_QUOTES);
          $yearsBlock = '<button id="'.$yearsData['label_name'].'_'.$yearsData['year'].'" class="btn btn-add" onclick=\'changeYear('.json_encode($yearsData, ENT_QUOTES).',"add");\' >'.$yearsData['year'].'</button>';
          break; */
        case 3:
            $yearsBlock = '<button id="data_' . $yearsData['year'] . '" class="btn btn-warned" onclick=\'changeYear(' . json_encode($yearsData, ENT_QUOTES) . ',"deactivate","' . $yearsData['label_name'] . '"); return false;\' >' . $yearsData['year'] . '</button>';
            break;
        case 4:
            $yearsBlock = '<button id="data_' . $yearsData['year'] . '" class="btn btn-warn" onclick=\'changeYear(' . json_encode($yearsData, ENT_QUOTES) . ',"activate","' . $yearsData['label_name'] . '"); return false;\' >' . $yearsData['year'] . '</button>';
            break;
    }

    return $yearsBlock;
}

function generateYearCarsDiv($carsData) {
    $carsContent = '<div>';
    if (isset($carsData['status']) && $carsData['status'] == 200) {
        $makesArray = array();
        $firstFlag = false;
        foreach ($carsData['data'] as $makes) {
            if (!isset($makesArray[$makes->make])) {
                $makesArray[$makes->make] = array();
            }
            if (!in_array($makes->model, $makesArray[$makes->make])) {
                array_push($makesArray[$makes->make], $makes->model);
            }
        }

        if (is_array($makesArray) && !empty($makesArray)) {
            $i = 1;
            $carsContent .= '<div class="bs-example"><div class="panel-group" id="accordion">';
            foreach ($makesArray as $make => $value) {
                /* if($firstFlag)
                  {
                  $carsContent .= '</li></ul>';
                  } */
                $carsContent .= '<div class="panel panel-default"><div class="panel-heading"><h4 class="panel-title"><div class="float_l"><input id="' . $i . '" type="checkbox" class="make" value="' . $make . '" /></div><a data-toggle="collapse" data-parent="#accordion" href="#collapse_' . $i . '"><div class="col-md-11 padtl" >&nbsp;' . $make . '</div></a><div class="clearfix"></div></h4></div><div id="collapse_' . $i . '" class="panel-collapse collapse out"><div class="panel-body"><div id="model_div_' . $i . '" class="row">';

                if (is_array($value) && !empty($value)) {
                    foreach ($value as $models) {
                        $carsContent .= '<div class="col-md-4"><input type="checkbox" class="model" value="' . $make . '#divide#' . $models . '" /><span class="marl5">' . $models . '</span></div>';
                    }
                }

                $firstFlag = true;
                $carsContent .= '</div></div></div></div>';
                $i++;
            }

            $carsContent .= '</div></div>';
        }
    } else {
        $carsContent = '<div>There are no cars avvailable to assign to this year.</div>';
    }

    $carsContent .= '</div>';
    return $carsContent;
}
