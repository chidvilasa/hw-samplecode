<?php

/*
 * This controller class handles user related functions
 * @author Chidvilasa
 * @Name ProductsController
 * 
 */

use Illuminate\Auth\UserTrait;
use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableTrait;
use Illuminate\Auth\Reminders\RemindableInterface;

class Products extends Eloquent implements UserInterface, RemindableInterface {

    use UserTrait,
        RemindableTrait;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cw_jobs';
    public $timestamps = true;

    /**
     * This function is used to get all products from database
     * @param  Integer $status
     * @return Array response
     */
    public function getProductsList($status = '') {
        $productsQuery = DB::table($this->table);
        $productsQuery->select('id', 'job_name', 'job_order', 'status', 'updated_at');
        if (!empty($status)) {
            $productsQuery->where('status', '=', DB::raw($status));
        }
        $productsQuery->orderBy('job_order', 'ASC');
        $productsQuery->orderBy('job_name', 'ASC');
        $products = $productsQuery->get();
        return $products;
    }

    /**
     * This function is used to get product details by id
     * @param  Integer $productId
     * @return Array response
     */
    public function getProductById($productId = '') {
        $product = array();
        if (!empty($productId)) {
            $product = DB::table($this->table)
                    ->select('id', 'job_name', 'job_order', 'status', 'created_at')
                    ->where('id', $productId)
                    ->get();
        }
        return $product;
    }

    /**
     * This function is used to update product details
     * @param  Array $data
     * @return Integer response
     */
    public function updateRecords($data = '') {
        $updateFlag = false;
        if (!empty($data)) {
            $updateFlag = DB::table($this->table)
                    ->where($data['upadateColumns'], $data['upadateValue'])
                    ->update($data['upadateData']);
        }

        return $updateFlag;
    }

    /**
     * This function is used to save new product details
     * @param  Array $data
     * @return Integer response
     */
    public function insertRecord($data = '') {
        $insertFlag = false;
        if (!empty($data)) {
            $insertFlag = DB::table($this->table)->insertGetId($data);
        }

        return $insertFlag;
    }

}
