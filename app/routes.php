<?php

/*
  |--------------------------------------------------------------------------
  | Application Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register all of the routes for an application.
  | It's a breeze. Simply tell Laravel the URIs it should respond to
  | and give it the Closure to execute when that URI is requested.
  |
 */

/* Products Section. Start */
Route::get('/products', 'ProductsController@products');
Route::post('/products/get_products', 'ProductsController@fetchProducts');
Route::post('/products/popup', 'ProductsController@openPopup');
Route::post('/products/update_products', 'ProductsController@modifyProducts');
Route::post('/change_status', 'ProductsController@changeStatus');
