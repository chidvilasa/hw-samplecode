<?php

/*
 * This controller class handles user related functions
 * @author Chidvilasa
 * @Name ProductsController
 * 
 */

class ProductsController extends Controller {

    /**
     * This function will open the Products home page, where all the activities take place.
     * @return html
     */
    public function products() {
        return View::make('products/products_home', array('pageTitle' => 'Products List'));
    }

    /**
     * This function is used to fetch all the products list and show the Grid page.
     * @return json response
     */
    public function fetchProducts() {
        $records = $this->getJobs();
        if (Request::ajax()) {
            $view = View::make('products.products_grid')->with('grid_data', $records)->render();
            return Response::json(array('data' => $view));
        }
    }

    /**
     * This function is used to handle the popup dialog request via ajax.
     * @return json response.
     */
    public function openPopup() {
        if (Request::ajax()) {
            $params = Input::all();
            $popupData['mode'] = $params['mode'];
            $popupData['form_id'] = 0;
            if ($params['mode'] == 2) {
                $productData = $this->getProduct($params['productId']);
                if (is_array($productData) && !empty($productData)) {
                    $popupData['product_details'] = $productData[0];
                    $popupData['form_id'] = $popupData['product_details']->id;
                }
            }

            $popup = View::make('products.popup_model')->with('product_data', $popupData)->render();
            return Response::json(array('dialog' => $popup));
        }
    }

    /**
     * This function is used to modify / update the product via the post request.
     * @return json response.
     */
    public function modifyProducts() {
        $post_data = Input::all();
        $output = array();
        if (is_array($post_data) && !empty($post_data)) {
            $output = $this->saveProducts($post_data);
        }
        return Response::json($output);
    }

    /**
     * This function is used to change product
     * @return json response.
     */
    public function changeStatus() {
        $data = Input::all();
        $updateFlag = false;
        if (!empty($data) && isset($data['recordId']) && !empty($data['recordId'])) {
            $objClass = Products::find($data['recordId']);
            $objClass->$data['column'] = $data['type'];
            $updateFlag = $objClass->save();
        }

        if ($updateFlag) {
            $this->return_array['status'] = 200;
            $this->return_array['message'] = 'Record updated successfully.';
        } else {
            $this->return_array['status'] = 100;
            $this->return_array['message'] = 'Problem in updating the record.';
        }

        return Response::json($this->return_array);
    }

    /**
     * This function will return all the jobs / products.
     * @return array
     */
    protected function getJobs($staus = '') {
        $products = with(new Products)->getProductsList($staus);

        return $products;
    }

    /**
     * This function will be used to save the products.
     * @param array $productsList
     * @return array
     */
    protected function saveProducts($productsList) {
        $this->return_array = array();
        $productArray['job_name'] = htmlEncode(trim($productsList['product_name']));
        $productArray['job_order'] = htmlEncode(trim($productsList['product_order']));
        if ($productsList['mode'] == 2 && $productsList['product_id'] != 0) {
            $productArray['updated_at'] = date('Y-m-d');
            $productData['upadateColumns'] = 'id';
            $productData['upadateValue'] = $productsList['product_id'];
            $productData['upadateData'] = $productArray;
            $updateFlag = with(new Products)->updateRecords($productData);
            unset($productArray);
        } else {
            $productArray['created_at'] = date('Y-m-d');
            $productArray['updated_at'] = date('Y-m-d');
            $updateFlag = with(new Products)->insertRecord($productArray);
        }

        if ($updateFlag) {
            $this->return_array['status'] = 200;
            $this->return_array['message'] = 'Record inserted or updated successfully.';
        } else {
            $this->return_array['status'] = 100;
            $this->return_array['message'] = 'There were no changes made. Please change and update.';
        }

        return $this->return_array;
    }

    /**
     * This function will return the specific product.
     * @param Integer $product
     * @return array
     */
    protected function getProduct($product) {
        $products = with(new Products)->getProductById($product);
        return $products;
    }

}
